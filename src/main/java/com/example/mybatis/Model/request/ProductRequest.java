package com.example.mybatis.Model.request;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProductRequest {
    private String productName;
    private double productPrice;
}
