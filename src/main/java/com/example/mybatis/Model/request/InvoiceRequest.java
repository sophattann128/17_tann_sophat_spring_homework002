package com.example.mybatis.Model.request;

import com.example.mybatis.Model.entities.Customer;
import com.example.mybatis.Model.entities.Product;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class InvoiceRequest {
    private String invoiceDate;
    private long customerId;
    private List<Long> productIds;
    public InvoiceRequest(long customerId, List<Long> productIds) {
        this.invoiceDate = new Date().toString();
        this.customerId = customerId;
        this.productIds = productIds;
    }
}
