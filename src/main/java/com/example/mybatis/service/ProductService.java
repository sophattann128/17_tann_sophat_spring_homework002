package com.example.mybatis.service;

import com.example.mybatis.Model.entities.Customer;
import com.example.mybatis.Model.entities.Product;
import com.example.mybatis.Model.request.ProductRequest;

import java.util.List;

public interface ProductService {
    List<Customer> getAllProduct();

    Product getProductById(long id);

    Product addNewProduct(ProductRequest productRequest);

    Product updateProductById(long id, ProductRequest productRequest);

    void deleteProductById(long id);
}
