package com.example.mybatis.service;

import com.example.mybatis.Model.entities.Customer;
import com.example.mybatis.Model.request.CustomerRequest;

import java.util.List;

public interface CustomerService {

    Customer addNewCustomer(CustomerRequest customerRequest);

    Customer getCustomerById(long id);

    List<Customer> getAllCustomer();

    void deleteCustomerById(long id);

    Customer updateCustomerById(long id, CustomerRequest customerRequest);
}
