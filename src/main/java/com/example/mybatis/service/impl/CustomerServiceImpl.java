package com.example.mybatis.service.impl;

import com.example.mybatis.Model.entities.Customer;
import com.example.mybatis.Model.request.CustomerRequest;
import com.example.mybatis.repository.CustomerRepository;
import com.example.mybatis.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {
    private final CustomerRepository customerRepository;
    @Override
    public Customer addNewCustomer(CustomerRequest customerRequest) {
        return customerRepository.addNewCustomer(customerRequest);
    }

    @Override
    public Customer getCustomerById(long id) {
        return customerRepository.getCustomerById(id);
    }

    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.getAllCustomer();
    }

    @Override
    public void deleteCustomerById(long id) {
        customerRepository.deleteCustomerById(id);
    }

    @Override
    public Customer updateCustomerById(long id, CustomerRequest customerRequest) {
          customerRepository.updateCustomerById(id, customerRequest);
          return customerRepository.getCustomerById(id);
    }
}
