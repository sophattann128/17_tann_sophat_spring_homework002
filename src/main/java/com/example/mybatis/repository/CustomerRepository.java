package com.example.mybatis.repository;

import com.example.mybatis.Model.entities.Customer;
import com.example.mybatis.Model.request.CustomerRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CustomerRepository {
    @Select("""
            insert into customer_tb (customer_name, customer_address, customer_phone) values (#{customerName}, #{customerAddress}, #{customerPhone} ) returning *;
                        
                        """)
    @Results(id = "customerResultMap", value = {
            @Result(property = "customerId", column = "customer_id"),
            @Result(property = "customerName", column = "customer_name"),
            @Result(property = "customerAddress", column = "customer_address"),
            @Result(property = "customerPhone", column = "customer_phone")

    })
    Customer addNewCustomer(CustomerRequest customerRequest);


    @Select("""
            select * from customer_tb where customer_id = #{id}
            """)
    @ResultMap("customerResultMap")
    Customer getCustomerById(@Param("id") long id);

    @Select("""
            select * from customer_tb
            """)
    @ResultMap("customerResultMap")
    List<Customer> getAllCustomer();

    @Delete("""
            delete from customer_tb where customer_id = #{id}
            """)
    @ResultMap("customerResultMap")
    void deleteCustomerById(long id);

    @Update("""
            update customer_tb set customer_name=#{cus.customerName},
                customer_address=#{cus.customerAddress}, customer_phone=#{cus.customerPhone}
            where customer_id = #{id};
            """)
    @ResultMap("customerResultMap")
    void updateCustomerById(long id,@Param("cus") CustomerRequest customerRequest);
}
