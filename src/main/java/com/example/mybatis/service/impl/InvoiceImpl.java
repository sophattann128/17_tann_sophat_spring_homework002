package com.example.mybatis.service.impl;

import com.example.mybatis.Model.entities.Invoice;
import com.example.mybatis.Model.request.InvoiceRequest;
import com.example.mybatis.repository.InvoiceRepository;
import com.example.mybatis.repository.ProductRepository;
import com.example.mybatis.service.InvoiceService;
import lombok.RequiredArgsConstructor;
import org.apache.ibatis.annotations.Insert;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class InvoiceImpl implements InvoiceService {
    private final InvoiceRepository invoiceRepository;
    private final ProductRepository productRepository;
    @Override
    public List<Invoice> getAllInvoice() {
        return invoiceRepository.getAllInvoice();
    }

    @Override
    public Invoice addNewInvoice(InvoiceRequest invoiceRequest) {
        long invoiceId = invoiceRepository.insertInvoiceWithCustomerAndProducts(invoiceRequest);
        return invoiceRepository.getInvoiceById(invoiceId);
    }

    @Override
    public Invoice getInvoiceById(long id) {
        return invoiceRepository.getInvoiceById(id);
    }

    @Override
    public void deleteInvoiceById(long id) {
        invoiceRepository.deleteInvoiceById(id);
    }

    @Override
    public Invoice updateInvoiceById(long id, InvoiceRequest invoiceRequest) {
        long invoiceId = invoiceRepository.updateInvoiceById(id, invoiceRequest);
       return invoiceRepository.getInvoiceById(invoiceId);
    }
}
