package com.example.mybatis.repository;

import com.example.mybatis.Model.entities.Customer;
import com.example.mybatis.Model.entities.Product;
import com.example.mybatis.Model.request.ProductRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ProductRepository {

    //    @Select("""
//            select pr.product_id, pr.product_name, pr.product_price from product_tb pr inner join invoice_tb it on pr.product_id = it.invoice_id
//             where invoice_id = #{invoiceId};
//
//
//
//                                            """)
//
//    List<Product> getProductByInvoiceId(@Param("invoiceId") long invoiceId);
//    SELECT pr.product_id, pr.product_name, pr.product_price
//    FROM product_tb pr
//    INNER JOIN invoice_tb it ON pr.product_id = it.invoice_id
//    WHERE it.invoice_id = #{invoiceId}
    @Select("""
               
                SELECT pr.product_id, pr.product_name, pr.product_price
                FROM product_tb pr
                         INNER JOIN invoice_detail_tb it ON pr.product_id = it.product_id
                WHERE it.invoice_id = #{invoiceId};
            """)
    List<Product> getProductByInvoiceId(@Param("invoiceId") long invoiceId);

    //    TODO it doesn't return
    @Select("""
            select * from product_tb
            """)
    @Results(id = "productResultMap", value = {
            @Result(property = "product_id", column = "productId"),
            @Result(property = "product_name", column = "productName"),
            @Result(property = "product_price", column = "productPrice"),
    }
    )
    List<Customer> getAllProduct();

    @Select("""
            select * from product_tb where product_id = #{id}
                        
            """)
    Product getProductById(long id);

    //    TODO it doesn't return
    @Select("""
            insert into product_tb (product_name, product_price) values (#{prod.productName}, #{prod.productPrice}) returning *;
                        
                        """)
    @ResultMap("productResultMap")
    Product addNewProduct(@Param("prod") ProductRequest productRequest);

    @Update("""
            update product_tb set product_name=#{prod.productName},
                product_price=#{prod.productPrice}
            where product_id = #{id};
            """)
    @ResultMap("productResultMap")
    void updateProductById(long id, @Param("prod") ProductRequest productRequest);

    @Delete("""
            delete from product_tb where product_id = #{id}
            """)
    @ResultMap("productResultMap")
    void deleteProductById(long id);
}
