package com.example.mybatis.controller;

import com.example.mybatis.Model.entities.Invoice;
import com.example.mybatis.Model.entities.Product;
import com.example.mybatis.Model.request.InvoiceRequest;
import com.example.mybatis.Model.response.ResponseHandler;
import com.example.mybatis.service.InvoiceService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/invoice")
@RequiredArgsConstructor
public class InvoiceController {
    private final InvoiceService invoiceService;
    private final ResponseHandler<Invoice> responseHandler;

    @GetMapping("/get-all-invoice")
    public ResponseEntity<?> getAllInvoice(){
        List<Invoice> invoices = invoiceService.getAllInvoice();
        System.out.println("Hello World");
        try {
            return ResponseEntity.ok(new ResponseHandler<>(
                    invoices,
                    "Successfully retrieved data",
                    true,
                    HttpStatus.OK.name()
            ));
        } catch (Exception e) {
            return responseHandler.generateResponse(e.getMessage(), false, HttpStatus.MULTI_STATUS.name());
        }

    }
    @PostMapping("/add-new-invoice")
    public ResponseEntity<?> addNewInvoice(@RequestBody InvoiceRequest invoiceRequest){
        try {
            Invoice addNewInvoice = invoiceService.addNewInvoice(invoiceRequest);
            return responseHandler.generateResponse(addNewInvoice, "Successfully created data", true, HttpStatus.OK.name());
        } catch (Exception e) {
            return responseHandler.generateResponse(e.getMessage(), false, HttpStatus.MULTI_STATUS.name());
        }
    }
    @GetMapping("/get-invoice-by-id/{id}")
    public ResponseEntity<?> getInvoiceById(@PathVariable long id){
        try {
            Invoice foundInvoice = invoiceService.getInvoiceById(id);
            if (foundInvoice != null) {
                return responseHandler.generateResponse(foundInvoice, "Product is found!", true, HttpStatus.OK.name());
            }
            return ResponseEntity.notFound().build();

        } catch (Exception e) {
            return responseHandler.generateResponse(e.getMessage(), false, HttpStatus.MULTI_STATUS.name());
        }
    }

    @PutMapping("/update-invoice-by-id/{id}")
    public ResponseEntity<?> updateInvoiceById(@PathVariable long id, @RequestBody InvoiceRequest invoiceRequest){
        try {
            Invoice invoiceToUpdate = invoiceService.getInvoiceById(id);
            if ( invoiceToUpdate!= null) {
                invoiceService.updateInvoiceById(id, invoiceRequest);
                return responseHandler.generateResponse( invoiceToUpdate,"Successfully updated data", true, HttpStatus.OK.name());
            }
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            return responseHandler.generateResponse(e.getMessage(), false, HttpStatus.MULTI_STATUS.name());
        }
    }

    @DeleteMapping("/delete-invoice-by-id/{id}")
    public ResponseEntity<?> deleteInvoiceById(@PathVariable long id){
        try {
            Invoice invoiceToDelete = invoiceService.getInvoiceById(id);
            if (invoiceToDelete != null) {
                invoiceService.deleteInvoiceById(id);
                return responseHandler.generateResponse("Successfully deleted data", true, HttpStatus.OK.name());
            }
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            return responseHandler.generateResponse(e.getMessage(), false, HttpStatus.MULTI_STATUS.name());
        }

    }



}
