package com.example.mybatis.Model.entities;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Product {
    private long productId;
    private String productName;
    private double productPrice;



}
