package com.example.mybatis.Model.response;

import com.example.mybatis.Model.entities.Customer;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@NoArgsConstructor
@Component
public class ResponseHandler<T> {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T payload;
    private String message;
    private boolean success;
    private String status;

    public ResponseHandler(T customer,String message,  boolean success, String status) {
        this.payload = customer;
        this.message = message;
        this.success = success;
        this.status = status;
    }


    public ResponseHandler(String message, boolean success, String status) {
        this.message = message;
        this.success = success;
        this.status = status;
    }

    public ResponseEntity<ResponseHandler<T>> generateResponse(T object,String message,  boolean success, String status){
        return ResponseEntity.ok(new ResponseHandler<>(object,message,success, status) );
    }
    public ResponseEntity<ResponseHandler<T>> generateResponse(String message, boolean success, String status){

        return ResponseEntity.ok(new ResponseHandler<>(message,success, status) );

    }
}
