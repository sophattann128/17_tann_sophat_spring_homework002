package com.example.mybatis.service.impl;

import com.example.mybatis.Model.entities.Customer;
import com.example.mybatis.Model.entities.Product;
import com.example.mybatis.Model.request.ProductRequest;
import com.example.mybatis.repository.ProductRepository;
import com.example.mybatis.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor

public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;
    @Override
    public List<Customer> getAllProduct() {
        return productRepository.getAllProduct();
    }

    @Override
    public Product getProductById(long id) {
        return productRepository.getProductById(id);
    }

    @Override
    public Product addNewProduct(ProductRequest productRequest) {
        return productRepository.addNewProduct(productRequest);
    }

    @Override
    public Product updateProductById(long id, ProductRequest productRequest) {
        productRepository.updateProductById(id, productRequest);
        return productRepository.getProductById(id);
    }

    @Override
    public void deleteProductById(long id) {
        productRepository.deleteProductById(id);
    }
}
