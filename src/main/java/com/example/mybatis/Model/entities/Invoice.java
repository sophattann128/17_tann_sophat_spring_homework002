package com.example.mybatis.Model.entities;

import jakarta.validation.constraints.NegativeOrZero;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Invoice {
    private long invoiceId;
    private String invoiceDate;
    private Customer customer;
    private List<Product> products;

}
