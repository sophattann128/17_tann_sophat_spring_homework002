package com.example.mybatis.service;

import com.example.mybatis.Model.entities.Invoice;
import com.example.mybatis.Model.request.InvoiceRequest;

import java.util.List;

public interface InvoiceService {
    List<Invoice> getAllInvoice();

    Invoice addNewInvoice(InvoiceRequest invoiceRequest);

    Invoice getInvoiceById(long id);

    void deleteInvoiceById(long id);

    Invoice updateInvoiceById(long id, InvoiceRequest invoiceRequest);
}
