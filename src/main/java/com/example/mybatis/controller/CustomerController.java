package com.example.mybatis.controller;

import com.example.mybatis.Model.entities.Customer;
import com.example.mybatis.Model.request.CustomerRequest;
import com.example.mybatis.Model.response.ResponseHandler;
import com.example.mybatis.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/customer")
public class CustomerController {
    private final CustomerService customerService;
    private final ResponseHandler<Customer> responseHandler;


    @PostMapping("/add-new-customer")
    public ResponseEntity<ResponseHandler<Customer>> addNewCustomer(@RequestBody CustomerRequest customerRequest) {
        try {
            Customer addCustomer = customerService.addNewCustomer(customerRequest);
            return responseHandler.generateResponse(addCustomer, "Successfully created data", true, HttpStatus.OK.name());
        } catch (Exception e) {
            return responseHandler.generateResponse(e.getMessage(), false, HttpStatus.MULTI_STATUS.name());
        }
    }

    @GetMapping("/get-customer-by-id/{id}")
    public ResponseEntity<ResponseHandler<Customer>> getCustomerById(@PathVariable long id) {
        try {
            Customer foundCustomer = customerService.getCustomerById(id);
            if (foundCustomer != null) {
                return responseHandler.generateResponse(foundCustomer, "Customer is found!", true, HttpStatus.OK.name());
            }
            return ResponseEntity.notFound().build();

        } catch (Exception e) {
            return responseHandler.generateResponse(e.getMessage(), false, HttpStatus.MULTI_STATUS.name());
        }
    }

    @GetMapping("/get-all-customer")
    public ResponseEntity<?> getAllCustomer() {
        List<Customer> customers = customerService.getAllCustomer();
        try {
            return ResponseEntity.ok(new ResponseHandler<>(
                    customers,
                    "Successfully retrieved data",
                   true,
                    HttpStatus.OK.name()
            ));
        } catch (Exception e) {
            return responseHandler.generateResponse(e.getMessage(), false, HttpStatus.MULTI_STATUS.name());
        }
    }

    @DeleteMapping("/delete-customer-by-id/{id}")
    public ResponseEntity<ResponseHandler<Customer>> deleteById(@PathVariable long id) {
        try {
            Customer customerToDelete = customerService.getCustomerById(id);
            if (customerToDelete != null) {
                customerService.deleteCustomerById(id);
                return responseHandler.generateResponse("Successfully deleted data", true, HttpStatus.OK.name());
            }
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            return responseHandler.generateResponse(e.getMessage(), false, HttpStatus.MULTI_STATUS.name());
        }
    }


    @PutMapping("/update-customer-by-id/{id}")
    public ResponseEntity<?> update(@PathVariable long id, @RequestBody CustomerRequest customerRequest) {
        try {
            Customer customerToUpdate = customerService.getCustomerById(id);
            if (customerToUpdate != null) {
                customerService.updateCustomerById(id, customerRequest);
                return responseHandler.generateResponse( customerToUpdate,"Successfully updated data", true, HttpStatus.OK.name());
            }
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            return responseHandler.generateResponse(e.getMessage(), false, HttpStatus.MULTI_STATUS.name());
        }
    }


}
