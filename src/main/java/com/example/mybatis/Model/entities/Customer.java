package com.example.mybatis.Model.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.awt.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Customer {
    private long customerId;
    private String customerName;
    private String customerAddress;
    private String customerPhone;
}
