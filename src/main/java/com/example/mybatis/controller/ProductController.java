package com.example.mybatis.controller;

import com.example.mybatis.Model.entities.Customer;
import com.example.mybatis.Model.entities.Product;
import com.example.mybatis.Model.request.ProductRequest;
import com.example.mybatis.Model.response.ResponseHandler;
import com.example.mybatis.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/product")
@RequiredArgsConstructor
public class ProductController {
    private final ProductService productService;
    private final ResponseHandler<Product> responseHandler;
    @GetMapping("/get-all-product")
    public ResponseEntity<?> getAllProduct(){
        List<Customer> customers = productService.getAllProduct();
        System.out.println(customers);
        try {
            return ResponseEntity.ok(new ResponseHandler<>(
                    customers,
                    "Successfully retrieved data",
                    true,
                    HttpStatus.OK.name()
            ));
        } catch (Exception e) {
            return responseHandler.generateResponse(e.getMessage(), false, HttpStatus.MULTI_STATUS.name());
        }

    }

    @GetMapping("/get-product-by-id/{id}")
    public ResponseEntity<?> getProductById(@PathVariable long id){
        try {
            Product foundProduct = productService.getProductById(id);
            if (foundProduct != null) {
                return responseHandler.generateResponse(foundProduct, "Product is found!", true, HttpStatus.OK.name());
            }
            return ResponseEntity.notFound().build();

        } catch (Exception e) {
            return responseHandler.generateResponse(e.getMessage(), false, HttpStatus.MULTI_STATUS.name());
        }
    }
    @PostMapping("/add-new-product")
    public ResponseEntity<?> addNewProduct(@RequestBody ProductRequest productRequest){
        try {
            Product addProduct = productService.addNewProduct(productRequest);
            System.out.println("AddProduct " + addProduct);

            return responseHandler.generateResponse(addProduct, "Successfully created data", true, HttpStatus.OK.name());
        } catch (Exception e) {
            return responseHandler.generateResponse(e.getMessage(), false, HttpStatus.MULTI_STATUS.name());
        }

    }

    @PutMapping("/update-product-by-id/{id}")
    public ResponseEntity<?> updateProduct(@PathVariable long id, @RequestBody ProductRequest productRequest){
        try {
            Product productToUpdate = productService.getProductById(id);
            if (productToUpdate != null) {
                productService.updateProductById(id, productRequest);
                return responseHandler.generateResponse( productToUpdate,"Successfully updated data", true, HttpStatus.OK.name());
            }
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            return responseHandler.generateResponse(e.getMessage(), false, HttpStatus.MULTI_STATUS.name());
        }
    }

    @DeleteMapping("/delete-product-by-id/{id}")
    private ResponseEntity<?> deleteProductById(@PathVariable long id){
        try {
            Product productToDelete = productService.getProductById(id);
            if (productToDelete != null) {
                productService.deleteProductById(id);
                return responseHandler.generateResponse("Successfully deleted data", true, HttpStatus.OK.name());
            }
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            return responseHandler.generateResponse(e.getMessage(), false, HttpStatus.MULTI_STATUS.name());
        }

    }



}
