package com.example.mybatis.repository;

import com.example.mybatis.Model.entities.Invoice;
import com.example.mybatis.Model.request.InvoiceRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface InvoiceRepository {

    @Select("SELECT * FROM invoice_tb")
    @Results(id = "invoiceResultMap", value = {
            @Result(property = "invoiceId", column = "invoice_id"),
            @Result(property = "invoiceDate", column = "invoice_date"),
            @Result(property = "customer", column = "customer_id",
                    one = @One(select = "com.example.mybatis.repository.CustomerRepository.getCustomerById")
            ),
            @Result(property = "products", column = "invoice_id",
                    many = @Many(select = "com.example.mybatis.repository.ProductRepository.getProductByInvoiceId")
            )
    })
    List<Invoice> getAllInvoice();

    @Select("""
            INSERT INTO invoice_tb (invoice_date, customer_id)
                        VALUES (#{in.invoiceDate}, #{in.customerId}) returning invoice_id;
            """)

    long insertInvoice(@Param("in") InvoiceRequest invoice);

    @Insert("""
            INSERT INTO invoice_detail_tb (invoice_id, product_id)
                        VALUES (#{invoiceId}, #{productId});
            """)
    void insertInvoiceProduct(@Param("invoiceId") long invoiceId, @Param("productId") long productId);
    default long insertInvoiceWithCustomerAndProducts(InvoiceRequest invoice) {

        long invoiceId = insertInvoice(invoice);
        for (long productId : invoice.getProductIds()) {
            insertInvoiceProduct(invoiceId, productId);
        }
        return invoiceId;
    }

    @Select("SELECT * FROM invoice_tb where invoice_id = #{id}")
    @ResultMap("invoiceResultMap")
    Invoice getInvoiceById(long id);

    @Delete("""
            delete from invoice_tb where invoice_id = #{id}
            """)
    @ResultMap("invoiceResultMap")
    void deleteInvoiceById(long id);

    @Update("""
            update invoice_tb set invoice_date = #{in.invoiceDate}, customer_id = #{in.customerId}
            where invoice_id = #{id};
            
            """)

    void updateInvoice(long id, @Param("in") InvoiceRequest invoice);

    @Insert("""
            update invoice_detail_tb set invoice_id = #{invoiceId}, product_id = #{in.productId}
            where invoice_id = #{id}
            """)
    void updateInvoiceProduct(@Param("invoiceId") long invoiceId, @Param("in") InvoiceRequest invoiceRequest);
    default long updateInvoiceById(long invoiceId, InvoiceRequest invoice) {
        updateInvoice(invoiceId,invoice);
        for (long productId : invoice.getProductIds()) {
            updateInvoiceProduct(productId, invoice);
        }
        return invoiceId;
    }

}
